<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**GitLab CLI Quick Start**

- [GitLab CLI quick start](#gitlab-cli-quick-start)
- [Installation](#installation)
- [Configuration](#configuration)
- [CLI basic usage](#cli-basic-usage)
- [Examples](#examples)
  - [Get project information](#get-project-information)
  - [Get project variables (e.g. CI/CD secrets)](#get-project-variables-eg-cicd-secrets)
  - [Project service integration](#project-service-integration)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# GitLab CLI quick start

This documentation get you started on GitLab CLI quickly. For full documentation, see [GitLab CLI](https://python-gitlab.readthedocs.io/en/stable/cli.html)

# Installation

```console
$ sudo pip install --upgrade python-gitlab
```

# Configuration

- Get your GitLab account API token from `https://code.stanford.edu/profile/personal_access_tokens`. The token should be scoped to `api`.

- Create ~/.python-gitlab.cfg with the content shown bellow:

```console
[global]
default = code.stanford.edu
ssl_verify = true
timeout = 10

[code.stanford.edu]
url = https://code.stanford.edu
private_token = <your gitlab token>
api_version = 4
```

# CLI basic usage

GitLab CLI requires two mandatory arguments: `objectType` and `action`. Here is an example to get available object and actions:

- List all supported GitLab objects

```console
$ gitlab -h |grep -A 1 ^object: | tail -1  | tr ',' '\n'

  {application-settings
broadcast-message
current-user
current-user-email
current-user-gp-gkey
current-user-key
deploy-key
dockerfile
event
feature
geo-node
gitignore
gitlabciyml
group
group-access-request
...}
```

- Get actions on an object

```console
$ gitlab -o json project --help | grep -A 1 ^action  | tail -1 | tr ',' '\n
{list
get
create
update
delete
repository-blob
repository-contributors
delete-merged-branches
share
archive
repository-compare
create-fork-relation
languages
mirror-pull
unarchive
star
search
artifact
trigger-pipeline
repository-archive
delete-fork-relation
repository-raw-blob
repository-tree
unstar
housekeeping
unshare
upload
snapshot
transfer-project}
```

# Examples

## Get project information

```console 
$ gitlab -o json project get --id=3454 | jq '.'
{
  "lfs_enabled": true,
  "request_access_enabled": false,
  "forks_count": 0,
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "http_url_to_repo": "https://code.stanford.edu/authnz/docker-certcache.git",
  "web_url": "https://code.stanford.edu/authnz/docker-certcache",
  "wiki_enabled": true,
  "id": 3454,
  "shared_with_groups": [],
  "archived": false,
  "snippets_enabled": true,
  "merge_method": "merge",
  "description": "",
  ...
}

```

## Get project variables (e.g. CI/CD secrets)

```console
$ gitlab -o json project-variable list --project-id=3454 | jq '.[]'
...
{
  "variable_type": "file",
  "protected": true,
  "key": "SOME_CREDENTIALS",
  "value": ...
  ...
}
```

## Project service integration

- Get available services 

```console
$ gitlab  -o json project-service  available --project-id=3454 --id all | jq
[
  "jira",
  "pipelines-email",
  "drone-ci",
  "flowdock",
  "buildkite",
  "gemnasium",
  "hipchat",
  "assembla",
  "redmine",
  "mattermost",
  "external-wiki",
  "pushover",
  "pivotaltracker",
  "teamcity",
  "custom-issue-tracker",
  "builds-email",
  "emails-on-push",
  "slack",
  "irker",
  "asana",
  "campfire",
  "bamboo"
]
```

- Get a specifica service integration

```console
$ gitlab  -o json project-service get  --project-id=3454 --id slack  | jq '.'
{
  "job_events": false,
  "confidential_note_events": false,
  "note_events": true,
  "updated_at": "2019-08-10T11:23:39.545-07:00",
  "confidential_issues_events": false,
  "issues_events": true,
  "push_events": true,
  "active": true,
  "id": 13116,
  "tag_push_events": true,
  "title": "Slack notifications",
  "created_at": "2019-08-08T09:58:13.522-07:00",
  "commit_events": true,
  "properties": {
    "username": "GitLab",
    "notify_only_broken_pipelines": "0",
    "tag_push_channel": "",
    "push_channel": "#authnz-git-commits",
    "merge_request_channel": "#authnz-git-commits",
    "webhook": "https://hooks.slack.com/services//...../.....",
    "pipeline_channel": "#authnz-build",
    "confidential_issue_channel": "#authnz-git-commits",
    "deployment_channel": "",
    "confidential_note_channel": "",
    "notify_only_default_branch": "0",
    "issue_channel": "#authnz-git-commits",
    "wiki_page_channel": "",
    "note_channel": ""
  },
  "merge_requests_events": true,
  "pipeline_events": true,
  "wiki_page_events": true
}
```
